# FreeDOS Helpers

Batch programs to perform tasks. Like the "Welcome to FreeDOS" message, CD/DVD-ROM initialization and more


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FDHELPER.LSM

<table>
<tr><td>title</td><td>FreeDOS Helpers</td></tr>
<tr><td>version</td><td>1.3.1</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-04-22</td></tr>
<tr><td>description</td><td>batch files to perform various tasks in FreeDOS</td></tr>
<tr><td>summary</td><td>Batch programs to perform tasks. Like the "Welcome to FreeDOS" message, CD/DVD-ROM initialization and more</td></tr>
<tr><td>keywords</td><td>boot cd dvd language message autoexec config system driver extension</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://gitlab.com/FDOS/base/fdhelper</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
